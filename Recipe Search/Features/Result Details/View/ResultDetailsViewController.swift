//
//  ResultDetailsViewController.swift
//  Recipe Search
//
//  Created by mac on 3/29/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import UIKit
import SafariServices

class ResultDetailsViewController: UIViewController {

    //MARK: - IBOutlets
    @IBOutlet private weak var recipeImage: UIImageView!
    @IBOutlet private weak var recipeTitle: UILabel!
    @IBOutlet private weak var recipeIngredients: UILabel!
    @IBOutlet private weak var linkButton: UIButton!
    
    //MARK: - Properties
    var detailsModel: ResultDetailsModel?
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: - IBOutlets
    @IBAction
    func didPressedLinkButton(_ sender: UIButton) {
        openSafariViewController()
    }
    

}

//MARK: - UI Setup
extension ResultDetailsViewController {
    
    fileprivate func setupUI() {
        title = "Item Details"
        setData()
        linkButton.layer.cornerRadius = linkButton.frame.height / 2
    }
    
    fileprivate func setData(){
        recipeImage.setImage(with: detailsModel?.recipeImage ?? "")
        recipeTitle.text = detailsModel?.recipeTitle
        recipeIngredients.text = detailsModel?.recipeIngredients
    }
    
    fileprivate func openSafariViewController(){
        guard let link = detailsModel?.recipeLink else {return}
        guard let url = URL(string: link) else { return }
        let svc = SFSafariViewController(url: url)
        present(svc, animated: true, completion: nil)
    }
    
    
}
