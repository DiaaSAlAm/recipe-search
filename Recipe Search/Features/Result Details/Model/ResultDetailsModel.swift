//
//  ResultDetailsModel.swift
//  Recipe Search
//
//  Created by mac on 3/29/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import Foundation

struct ResultDetailsModel {
    var recipeImage: String
    var recipeTitle: String
    var recipeIngredients: String
    var recipeLink: String
    
    init(recipeImage: String,recipeTitle: String, recipeIngredients: String,recipeLink: String) {
        self.recipeImage = recipeImage
        self.recipeTitle = recipeTitle
        self.recipeIngredients = recipeIngredients
        self.recipeLink = recipeLink
    }
    
}
