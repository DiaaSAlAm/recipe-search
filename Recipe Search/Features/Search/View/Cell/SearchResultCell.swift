//
//  SearchResultCell.swift
//  Recipe Search
//
//  Created by mac on 3/28/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import UIKit

class SearchResultCell: UICollectionViewCell {
    
    //MARK: - IBOutlets
    
    @IBOutlet weak var resultImageView: UIImageView!
    @IBOutlet weak var recipeTitle: UILabel!
    @IBOutlet weak var scource: UILabel!
    @IBOutlet weak var recipeHealthLabels: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
