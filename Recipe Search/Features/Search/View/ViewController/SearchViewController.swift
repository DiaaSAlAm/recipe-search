//
//  SearchViewController.swift
//  Recipe Search
//
//  Created by mac on 3/28/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {
    
    //MARK: - IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    //MARK: - Properties
    lazy var viewModel: SearchViewModel = {
        return SearchViewModel()
    }()
    
    private let recentSearchCell = "RecentSearchCell"
    private let searchResultCell = "SearchResultCell"
    private let activityView = UIActivityIndicatorView(style: .gray)
    private let fadeView: UIView = UIView()
    private var width : CGFloat = CGFloat()
    private var numberOfRow = 0
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
}

//MARK: - UI Setup
extension SearchViewController {
    
    fileprivate func setupUI() {
        title = "Recipe Search"
        viewModel.delegate = self
        searchBar.delegate = self
        width = self.view.frame.width - 16
        let searchBarStyle = searchBar.value(forKey: "searchField") as? UITextField
        searchBarStyle?.clearButtonMode = .never
        hideKeyboardWhenTappedAround()
        registerCollectionView()
        viewModel.handleCollectionState(numberOfItems: 0, width: width , collectionView: collectionView)
    }
    
    //MARK: - Start Animating Activity
    fileprivate func startAnimatingActivityView() {
        fadeView.frame = self.collectionView.frame
        fadeView.backgroundColor = .white
        fadeView.alpha = 0.6
        self.view.addSubview(fadeView)
        self.view.addSubview(activityView)
        activityView.hidesWhenStopped = true
        activityView.center = self.collectionView.center
        activityView.startAnimating()
    }
    
    //MARK: - Stop Animating Activity
    fileprivate func stopAnimatingActivityView() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 1, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                self.collectionView?.alpha = 1
                self.fadeView.removeFromSuperview()
                self.activityView.stopAnimating()
            }, completion: nil)
        }
    }
    
    //MARK: - Register  CollectionView Cell
    fileprivate func registerCollectionView(){
        collectionView.register(UINib(nibName: recentSearchCell, bundle: nil), forCellWithReuseIdentifier: recentSearchCell)
        collectionView.register(UINib(nibName: searchResultCell, bundle: nil), forCellWithReuseIdentifier: searchResultCell)
        
    }
    
    //MARK: Go To Details and Pass Model Data
   fileprivate func goToDetails(model: ResultDetailsModel){
        let storyboard = UIStoryboard.init(name: "SearchResultDetails", bundle: nil)
        guard let viewController = storyboard.instantiateViewController(withIdentifier: "ResultDetailsViewController") as? ResultDetailsViewController else { return }
        viewController.detailsModel = model
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
}


//MARK: - UICollectionView Delegate
extension SearchViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        defer { collectionView.deselectItem(at: indexPath, animated: true) }
        switch viewModel.selectedSearchType {
        case .recentSearch:
            let searchBarText = viewModel.getRecentSearch(indexPath.row)
            viewModel.fetchSearchRecipeResult(searchText:searchBarText , from: 0, to: 10, append: false)
            viewModel.insertNewRecipe_To_RecentSearch(recipeTitle: searchBarText)
            searchBar.text = searchBarText //Change Search bar test to selected item text
        default:
            let model = ResultDetailsModel.init(recipeImage: viewModel.getResultImage(indexPath.row), recipeTitle: viewModel.getRecipeTitle(indexPath.row), recipeIngredients: viewModel.getRecipeIngredients(indexPath.row), recipeLink: viewModel.getRecipeLink(indexPath.row))
            goToDetails(model: model)
            break
        }
    }
}

//MARK: - UICollectionView Data Source
extension SearchViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        switch viewModel.selectedSearchType {
        case .recentSearch:
            return numberOfRow
        case .resultSearch:
            return viewModel.numberOfHits()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch viewModel.selectedSearchType {
        case .recentSearch:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: recentSearchCell,for: indexPath) as! RecentSearchCell
            cell.recentSearchLabel.text = recentSearchText.getElement(at: indexPath.row)
            return cell
        case .resultSearch:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: searchResultCell,for: indexPath) as! SearchResultCell
            cell.resultImageView.setImage(with: viewModel.getResultImage(indexPath.row))
            cell.recipeTitle.text = viewModel.getRecipeTitle(indexPath.row)
            cell.scource.text =  viewModel.getSource(indexPath.row)
            cell.recipeHealthLabels.text = viewModel.getRecipeHealthLabels(indexPath.row)
            return cell
        }
    }
}

//MARK: - UICollectionView Delegate Flow Layout
extension SearchViewController: UICollectionViewDelegateFlowLayout {
    
    //MARK: - Load new item if Available
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        if viewModel.selectedSearchType == .resultSearch, viewModel.more && indexPath.item == viewModel.numberOfHits() - 1 {
            viewModel.fetchSearchRecipeResult(searchText: viewModel.searchText, from: viewModel.from + 10, to: viewModel.to + 10, append: true)
        }
        
    }
    
    //MARK: - change size For Item Dependence on CollectionView type
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        switch viewModel.selectedSearchType {
        case .recentSearch:
            return CGSize(width: width - 16, height: 40)
        case .resultSearch:
            return CGSize(width: width - 16, height: 345)
        }
    }
}

//MARK: Search Bar Delegate
extension SearchViewController : UISearchBarDelegate {
    
    //MARK: - Search Button Clicked
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchBarText = searchBar.text, !searchBar.text!.isEmptyOrWhitespace() else {return}
        viewModel.insertNewRecipe_To_RecentSearch(recipeTitle: searchBarText)
        viewModel.fetchSearchRecipeResult(searchText: searchBarText, from: 0, to: 10, append: false)
        view.endEditing(true) //Dissmiss Keyboard
        
    }
    
    //MARK: - Cancel Button Clicked -> Reload Last Data Fetched
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.reloadLastDataFetched(searchBar: searchBar, width: width, collectionView: collectionView)
    }
    
    //MARK: - SearchBar Text Did Begin Editing -> Show Recent Suggestions
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        viewModel.showRecentSearchRow(width: width, collectionView: collectionView)
        numberOfRow = viewModel.numberOfRecentSearch()
    }
}

//MARK: - Main Protcol
extension SearchViewController : MainProtocol {
    
    func onError(errorMsg: String) {
        showToast(message:  errorMsg )//"Connection Error")
    }
    
    func startLoading() {
        startAnimatingActivityView()
    }
    
    func stopLoading() {
        stopAnimatingActivityView()
    }
    
    func successRequest(append: Bool) {
        switch append {
        case true:
            viewModel.reloadData(.resultSearch, options: .curveEaseInOut, numberOfItems:  viewModel.numberOfHits(), width: width, collectionView: collectionView)
        default:
            viewModel.reloadData(.resultSearch, options: .transitionFlipFromRight, numberOfItems:  viewModel.numberOfHits(), width: width, collectionView: collectionView)
        } 
    }
}

