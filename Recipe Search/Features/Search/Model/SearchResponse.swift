//
//  SearchResponse.swift
//  Recipe Search
//
//  Created by mac on 3/28/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import Foundation

struct SearchResponse : Codable {
    let q : String
    let from : Int
    let to : Int
    let more : Bool
    let count : Int
    let hits : [Hits]
}


struct SearchResponseError : Codable {
  
    let status: String
    let message: String
}
