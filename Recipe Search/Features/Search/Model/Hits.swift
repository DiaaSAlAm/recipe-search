//
//  Hits.swift
//  Recipe Search
//
//  Created by mac on 3/28/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import Foundation

struct Hits : Codable {
    let recipe : Recipe
    let bookmarked : Bool
    let bought : Bool
}
