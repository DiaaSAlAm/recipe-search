//
//  Recipe.swift
//  Recipe Search
//
//  Created by mac on 3/28/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import Foundation

struct Recipe : Codable {
    let label : String
    let image : String
    let source : String
    let url : String
    let healthLabels : [String]
    let ingredientLines : [String]
}
