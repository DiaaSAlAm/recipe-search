//
//  SearchViewModel.swift
//  Recipe Search
//
//  Created by mac on 3/28/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import UIKit
import Alamofire

class SearchViewModel {
    
    //MARK: - enum for Switch Data type in Collection View
    enum DataType {
        case recentSearch
        case resultSearch
    }
    
    //MARK: - Properties
    var delegate: MainProtocol?
    var searchResponse : SearchResponse?
    var hitsResponse : [Hits] = []
    var searchText: String { return searchResponse?.q ?? "" }
    var from: Int { return searchResponse?.from ?? 0 }
    var to: Int { return searchResponse?.to ?? 0 }
    var more: Bool { return searchResponse?.more ?? false }
    var selectedSearchType: DataType = .recentSearch
    
    //MARK: Number OF Row -> Recent Search
    func numberOfRecentSearch() -> Int {
        return recentSearchText.count
    }
    
    //MARK: Recent Search Recipe
    func getRecentSearch(_ row: Int) -> String {
        return recentSearchText[row]
    }
    
    func insertNewRecipe_To_RecentSearch(recipeTitle: String){
        
        if let recipeIndex = recentSearchText.firstIndex(of: recipeTitle) { // Replce elment index from element postion to first index
            recentSearchText.remove(at: recipeIndex)
        }
        
        recentSearchText.insert(recipeTitle, at: 0) // insert elment at 0 index
        
        if numberOfRecentSearch() > 10 { // Show last 10 recent Search
            recentSearchText.removeLast()
        }
        
        saveRecentSearchOnUserDefaults() // Save Recent Suggetions on User Defaults
    }
    
    func saveRecentSearchOnUserDefaults(){
        prefs.set(recentSearchText, forKey: UD.PrefKeys.recentSearchText)
    }
    
    //MARK: Number OF Row -> Search Result Hits
    func numberOfHits() -> Int {
        return hitsResponse.count
    }
    
    //MARK: Search Result Data
    func getResultImage(_ row: Int) -> String {
        return hitsResponse.getElement(at: row)?.recipe.image ?? ""
    }
    
    func getRecipeTitle(_ row: Int) -> String {
        return hitsResponse.getElement(at: row)?.recipe.label ?? ""
    }
    
    func getSource(_ row: Int) -> String {
        return hitsResponse.getElement(at: row)?.recipe.source ?? ""
    }
    
    func getRecipeHealthLabels(_ row: Int) -> String {
        return hitsResponse.getElement(at: row)?.recipe.healthLabels.joined(separator: ", ") ?? ""
    }
    
    func getRecipeIngredients(_ row: Int) -> String {
        return hitsResponse.getElement(at: row)?.recipe.ingredientLines.joined(separator: " \n") ?? ""
    }
    
    func getRecipeLink(_ row: Int) -> String {
        return hitsResponse.getElement(at: row)?.recipe.url ?? ""
    }
    
}


//MARK: Setup CollectionView
extension SearchViewModel{
   //MARK: - handle label on background collectionView
      func handleCollectionState(numberOfItems: Int,width: CGFloat, collectionView: UICollectionView ) {
          if numberOfItems == 0 {
              let frame = CGRect(x: 0, y: 0, width: width, height: 30)
              let label = UILabel(frame: frame)
              label.text = "No Data Available"
              label.textColor = .darkGray
              label.textAlignment = .center
              label.center = collectionView.center
              label.sizeToFit()
              collectionView.backgroundView = label
          } else {
              collectionView.backgroundView = nil
          }
      }
      
      //MARK: - reload Data Dependence on CollectionView type
     func reloadData(_ type: DataType, options: UIView.AnimationOptions, numberOfItems: Int,width: CGFloat, collectionView: UICollectionView ) {
          selectedSearchType = type
          handleCollectionState(numberOfItems: numberOfItems, width: width , collectionView: collectionView)
          UIView.transition(with: collectionView, duration: 0.5, options: options, animations: {
              //Do the data reload here
              collectionView.reloadData()
          }, completion: nil)
      }
    
    func reloadLastDataFetched(searchBar: UISearchBar, width: CGFloat, collectionView: UICollectionView ) {
        searchBar.text = searchText
        reloadData(.resultSearch, options: .transitionFlipFromRight, numberOfItems: numberOfHits(), width: width, collectionView: collectionView)
    }
    
    func showRecentSearchRow(width: CGFloat, collectionView: UICollectionView) {
//        numberOfRow = numberOfRecentSearch()
        reloadData(.recentSearch, options: .transitionFlipFromLeft, numberOfItems: numberOfRecentSearch(), width: width, collectionView: collectionView)
    }
    
}

//MARK: Service
extension SearchViewModel {
    //Fetch Search Result
    func fetchSearchRecipeResult(searchText: String, from: Int, to: Int, append: Bool) {
        delegate?.startLoading()
        Alamofire.request(APIRouter.search(searchText: searchText, from: from, to: to))
            .responseJSON {[weak self] (response) in
                guard let self = self else { return }
                defer { self.delegate?.stopLoading()}
                    if let jsonData = response.data {
                        let jsonDecoder = JSONDecoder()
                        do {
                            let model = try jsonDecoder.decode(SearchResponse.self, from: jsonData)
                            self.searchResponse = model
                            self.hitsResponse = append ? (self.hitsResponse + model.hits) : model.hits
                            self.delegate?.successRequest(append: append)
                        }catch{
                            do {
                                 let model = try jsonDecoder.decode(SearchResponseError.self, from: jsonData)
                                self.delegate?.onError(errorMsg: model.message)
                            }catch{
                                 self.delegate?.onError(errorMsg: "Connection Error")
                            }
                        }
                    }
        }
    }
}
