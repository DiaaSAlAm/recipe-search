//
//  AppArgument.swift
//  Recipe Search
//
//  Created by mac on 3/28/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import Foundation

struct AppArgument {
    static func runForUnitTesting() -> Bool {
        return CommandLine.arguments.contains("-unittesting")
    }
}
