//
//  AppContext.swift
//  Recipe Search
//
//  Created by mac on 3/28/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import Foundation

struct AppContext {
    
    static var environment: Environment = .dev
    
    static func setupAppEnvironment() {
        #if DEBUG || RELEASE
        if AppArgument.runForUnitTesting() {
            //environment = .custom(url: "localhost", port: 9999)
        } else {
            environment = .dev
        }
        #elseif AppStore
        environment = .prod
        #endif
    }
    
}
