//
//  AppStarter.swift
//  Recipe Search
//
//  Created by mac on 3/28/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import UIKit

/// AppStarter here you can handle everything before letting your app starts
final class AppStarter {
    static let shared = AppStarter()
    
    private init() {}
    
    func start(window: UIWindow?) {
        AppContext.setupAppEnvironment()
        AppTheme.apply()
        setRootViewController(window: window)
    }
    
    private func setRootViewController(window: UIWindow?) {
        let storyboard = UIStoryboard(name: "Search", bundle: nil)
        let searchViewController = storyboard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        let rootViewController = UINavigationController(rootViewController: searchViewController)
        window?.rootViewController = rootViewController
        window?.makeKeyAndVisible()
    }
}
