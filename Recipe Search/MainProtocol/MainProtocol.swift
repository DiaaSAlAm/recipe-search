//
//  MainProtocol.swift
//  Recipe Search
//
//  Created by mac on 3/28/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import Foundation

protocol MainProtocol {
    func onError(errorMsg:String)
    func startLoading()
    func stopLoading()
    func successRequest(append: Bool)
}
