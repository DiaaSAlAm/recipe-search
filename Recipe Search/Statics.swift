//
//  Statics.swift
//  Recipe Search
//
//  Created by mac on 3/28/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

var recentSearchText : [String] {
    get {
        return prefs.stringArray(forKey: UD.PrefKeys.recentSearchText) ?? []
    }
    set {
        prefs.set(newValue, forKey: UD.PrefKeys.recentSearchText)
    }
}
