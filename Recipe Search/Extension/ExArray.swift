//
//  ExArray.swift
//  Recipe Search
//
//  Created by mac on 3/28/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import Foundation

extension Array {
    func getElement(at index: Int) -> Element? {
        let isValidIndex = index >= 0 && index < count
        return isValidIndex ? self[index] : nil
    }
}
