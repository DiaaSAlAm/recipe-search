//
//  Environment.swift
//  Recipe Search
//
//  Created by mac on 3/28/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//

import Foundation

public enum Environment {
    case prod
    case dev
    case custom(url: String, port: Int)
    
    var baseURL: String {
        switch self {
        case .prod:
            return "Empty -----"
        case .dev:
            return "https://api.edamam.com/"
        case .custom(let url, _):
            return url
        }
    }
}
