//
//  APIRouter.swift
//  Recipe Search
//
//  Created by mac on 3/28/20.
//  Copyright © 2020 Diaa Salam. All rights reserved.
//


import Alamofire

enum APIRouter: URLRequestConvertible {
    
    //MARK: - Search Case
    
    case search(searchText: String, from: Int, to: Int)
    
    //MARK: - Set HTTP Method
    
    var method: HTTPMethod {
        switch self {
        case .search:
            return .get
        }
    }
    
    //MARK: - Set Path to URL
    
    var path: String {
        switch self {
        case .search:
            return "search"
        }
    }
    
    //MARK: URLRequestConvertible
    
    func asURLRequest() throws -> URLRequest {
        var params = Parameters()
        let app_id = "89193820"
        let app_key = "b0c4f8afaa13f79ee3219775e0d90d23"
        let url = try Environment.dev.baseURL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(path))
        urlRequest.httpMethod = method.rawValue
        
        //MARK: - Set Parameters
        switch self {
        case .search(let searchText,let from,let to):
            params = [
                "q": searchText,
                "from": from,
                "to" : to,
                "app_id": app_id,
                "app_key" : app_key
            ]
            urlRequest = try  URLEncoding(boolEncoding: .literal).encode(urlRequest, with: params)
        }
        return urlRequest
    }
}
